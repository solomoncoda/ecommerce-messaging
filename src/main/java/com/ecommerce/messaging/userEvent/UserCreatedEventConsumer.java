package com.ecommerce.messaging.userEvent;

import com.ecommerce.messaging.models.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;


public class UserCreatedEventConsumer {

    private Logger logger = LoggerFactory.getLogger(UserCreatedEventConsumer.class);
    @Autowired
    private Environment env;

    @RabbitListener(queues="userServiceQueue")
    public void receive(String message) throws JsonProcessingException {
        logger.info("Received message '{}'", message);

        final String uri = env.getProperty("mailserver.url") + "/mail/userCreatedEmail";

        logger.info("MailServer URI '{}'", uri);

        RestTemplate restTemplate = new RestTemplate();
        User user = new ObjectMapper().readValue(message, User.class);
        restTemplate.postForEntity(uri, user.getEmail(), String.class);
    }
}
