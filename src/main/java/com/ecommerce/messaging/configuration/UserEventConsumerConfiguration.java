package com.ecommerce.messaging.configuration;

import com.ecommerce.messaging.userEvent.UserCreatedEventConsumer;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserEventConsumerConfiguration {
    @Bean
    public Exchange eventExchange() {
        return new TopicExchange("newUserEventExchange");
    }

    @Bean
    public Queue queue() {
        return new Queue("userServiceQueue");
    }

    @Bean
    public Binding binding(Queue queue, TopicExchange eventExchange) {
        return BindingBuilder
                .bind(queue)
                .to(eventExchange)
                .with("user.*");
    }

    @Bean
    public UserCreatedEventConsumer eventReceiver() {
        return new UserCreatedEventConsumer();
    }

}
