package com.ecommerce.messaging.models;
import java.io.Serializable;

public class User implements Serializable {

    private Integer userId;
    private String userName;
    private String password;
    private String postalAddress;
    private String contactNumber;
    private String email;

    public User() {
    }

    public User(Integer userId, String userName, String password, String postalAddress, String contactNumber, String email) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
        this.postalAddress = postalAddress;
        this.contactNumber = contactNumber;
        this.email = email;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", postalAddress='" + postalAddress + '\'' +
                ", contactNumber='" + contactNumber + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}

